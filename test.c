#include <stdio.h>
#include <mpi.h>
#include "inputParser.h"
#include "neighbor.h"

int main(int argc, char **argv)
{
    MPI_Init(&argc, &argv);
    void inputTest(int argc, char **argv);
    void neighTest(NBR *neighbor, MPI_Comm comm);
    void neighUpdateTest(NBR *neighbor, int fail_rank, int new_rank, MPI_Comm
    comm);

    PARAMETERS para = processInputs(argc, argv); 
    NBR neighbor;
    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    neighTest(&neighbor, MPI_COMM_WORLD);
    neighUpdateTest(&neighbor, 1, -1, MPI_COMM_WORLD); /* rank 1 fails, shrinking */
    neighUpdateTest(&neighbor, 2, size, MPI_COMM_WORLD);

    MPI_Finalize();
    return 0;
}

void inputTest(int argc, char **argv)
{
    PARAMETERS para = processInputs(argc, argv);

    printf("arraySize = %d\n", para.arraySize);
    printf("maxLoop = %d\n", para.maxLoop);
    printf("ncomp = %d\n", para.ncomp);
    printf("ncomm_neigh = %d\n", para.ncomm_neigh);
    printf("ncomm_point = %d\n", para.ncomm_point);
    printf("ncomm_coll = %d\n", para.ncomm_coll);
    printf("recmode = %d\n", para.recmode);
    printf("lbmode = %d\n", para.lbmode);

}

void neighTest(NBR *neighbor, MPI_Comm comm) 
{
    int rank, i;
    MPI_Comm_rank(comm, &rank);
    neighborSetup(neighbor, comm);
    printf("My rank is %d, and my neighbors are", rank);
    for(i = 0; i < neighbor->n_neighbor; i++) {
        printf(" %d", neighbor->nbr_rank[i]);
    }
    printf("\n");
}

void neighUpdateTest(NBR * neighbor, int fail_rank, int new_rank, MPI_Comm comm)
{
    int rank, i;
    MPI_Comm_rank(comm, &rank);
    
    neighborUpdate(neighbor, fail_rank, new_rank);

    printf("My rank is %d, after update my neighbors are", rank);
    for(i = 0; i < neighbor->n_neighbor; i++) {
        printf(" %d", neighbor->nbr_rank[i]);
    }
    printf("\n");
}
