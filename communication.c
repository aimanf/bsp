#include "bsp.h"
#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

void nbr_comm(MPI_Comm comm, NBR *nbr, int *sendbuf, int *recvbuf, int
msgSize)
{
    int i, j;
    MPI_Request reqList[2 * MAXNBR];
    int rank;
    MPI_Comm_rank(comm, &rank);
    //printf("rank %d call reqList with neighbors %d\n", rank, nbr->n_nbr);
    //reqList = (MPI_Request*)malloc(2 * nbr->n_nbr * sizeof(MPI_Request));

    for (i = 0; i < nbr->n_nbr; i++) {
        MPI_Isend(sendbuf, msgSize, MPI_INT, nbr->nbr_rank[i], 0, comm,
        &reqList[i]);        
        //printf("myrank is %d and send to %d\n", rank, nbr->nbr_rank[i]);
    }

    for (j = 0; j < nbr->n_nbr; j++) {
        MPI_Irecv(recvbuf, msgSize, MPI_INT, nbr->nbr_rank[j], 0,
        comm, &reqList[i++]);
        //printf("myrank is %d and rec from %d\n", rank, nbr->nbr_rank[i]);
    }
    
    MPI_Waitall(2*nbr->n_nbr, reqList, MPI_STATUSES_IGNORE);
    //printf("rank %d complete neighbor comm\n", rank);
}

void ptp_comm(MPI_Comm workComm, int * sendbuf, int * recvbuf, int msgSize)
{
    int rank, size, *sendlist, *recvlist;
    int i, nreq = 0;
    MPI_Request *req;
    MPI_Comm_rank(workComm, &rank);
    MPI_Comm_size(workComm, &size);

    sendlist = (int *)malloc(size * sizeof(int));
    recvlist = (int *)malloc(size * sizeof(int));
    req = (MPI_Request *)malloc(size * sizeof(MPI_Request));

    if (rank == 0) {
        for (i = 0; i < size; i++) {
            srand(time(NULL));
            sendlist[i] = rand() % size;
            recvlist[i] = rand() % size;
            if (recvlist[i] == sendlist[i])
                recvlist[i] = (recvlist[i] + 1) % size;
        }
    }
    MPI_Bcast(sendlist, size, MPI_INT, 0, workComm);
    MPI_Bcast(recvlist, size, MPI_INT, 0, workComm);

    for (i = 0; i < size; i++) {
        if (rank == sendlist[i]) {
            MPI_Isend(sendbuf, msgSize, MPI_INT, recvlist[i], 0, workComm, &req[nreq++]);
        } else if (rank == recvlist[i]) {
            MPI_Irecv(recvbuf, msgSize, MPI_INT, sendlist[i], 0, workComm, &req[nreq++]);
        }
    }
    
    MPI_Waitall(nreq, req, MPI_STATUS_IGNORE);

    free(sendlist);
    free(recvlist);
    free(req);
}

void coll_comm(MPI_Comm workComm, int *sendbuf, int *recvbuf, int msgSize)
{
    int size, rank;
    MPI_Comm_rank(workComm, &rank);
    MPI_Comm_size(workComm, &size);

    MPI_Allreduce(sendbuf, recvbuf, msgSize, MPI_INT, MPI_SUM, workComm);
}
