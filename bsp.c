#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <stdbool.h>
#include "neighbor.h"
#include "inputParser.h"
#include "load.h"
#include "bsp.h"

bool is_failRank(int rank, int nFailure, int * failRank)
{
    int i;

    for (i = 0; i < nFailure; i++) {
        if (rank == failRank[i])
            return true;
    }
    return false;
}

int main(int argc, char **argv)
{
    int rank, size, newrank, spareSize, workSize, arraySize;
    PARAMETERS para;
    NBR nbr;
    int *array, *recvbuf, *sendbuf; 
    int *workRanks, *spareRanks;
    int nbrRank[MAXNBR]; /* neighbor rank in MPI_COMM_WORLD */
    int i, j, k;
    double begin, end, Ta, Tb, T_cmp = 0, T_cmm = 0, T_neigh_comm = 0,
    T_ptp_comm = 0, T_coll_comm = 0;
    double T_cmp_b = 0, T_cmp_a = 0;
    double T_comm_b = 0, T_comm_a = 0;
    MPI_Group workGroup, worldGroup;
    MPI_Comm workComm;
    MPI_Status status;
    bool workMem = false;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    /* get parameters from input file */
    para = processInputs(argc, argv);

    int spaceSize = 1;
    if (para.space.x > 0) spaceSize *= para.space.x;
    if (para.space.y > 0) spaceSize *= para.space.y;
    if (para.space.z > 0) spaceSize *= para.space.z;

    if (spaceSize < para.workSize) {
        fprintf(stderr, "Space size is smaller than work size.\n");
        return 1;
    } else if (spaceSize < para.workSize + para.spareSize) {
        fprintf(stderr, "Space size is smaller than work size + spare "
        "size.\n");
        return 1;
    }
    /*
    if (rank == 0) printf("%d failed ranks:", para.nFailure);
    for (i = 0; i < para.nFailure; i++) {
        if (rank == 0) printf(" %d", para.failRank[i]);
    }
    if (rank == 0) printf("\n");
    */

    workSize = para.workSize;
    spareSize = para.spareSize;
    arraySize = para.arraySize;

    if (rank < para.workSize) {
        workMem = true; // It's a member of work group
    }

    /* intialize array */
    array = (int *)malloc(arraySize * sizeof(int));
    recvbuf = (int *)malloc(para.msgSize * sizeof(int));
    sendbuf = (int *)malloc(para.msgSize * sizeof(int));

    for (i = 0; i < arraySize; i++) {
        array[i] = rank;
    }
    for (i = 0; i < para.msgSize; i++) {
        recvbuf[i] = 0;
        sendbuf[i] = 1;
    }

    /* Setup work group 
     * Initially work group includes ranks from 1 to n
     */
    MPI_Comm_group(MPI_COMM_WORLD, &worldGroup);
    workRanks = (int *)malloc(workSize * sizeof(int));
    spareRanks = (int *)malloc(spareSize * sizeof(int));
   
    /*
    if (rank == 0)
        printf("workSize = %d, spareSize = %d\n", workSize, spareSize);
    */
    for (i = 0; i < para.workSize; i++) {
        workRanks[i] = i;
    }
    for (j = 0; j < para.spareSize; j++, i++) {
        spareRanks[j] = i;
    }
    MPI_Group_incl(worldGroup, workSize, workRanks, &workGroup);
    MPI_Comm_create(MPI_COMM_WORLD, workGroup, &workComm);

    /* Setup neighbor list */
    if (workMem)
        neighborSetup(&nbr, nbrRank, para.space, worldGroup, workGroup);

    MPI_Barrier(MPI_COMM_WORLD);
    
    /* Main loop */
    begin = MPI_Wtime();
    for (i = 1; i <= para.maxLoop; i++) {
        if (workMem) { /* only member in workGroup commpute and communicate */
            /* computation */
            double cmp_begin = MPI_Wtime();
            for (j = 0; j < para.ncomp; j++) {
                computation(array, arraySize, recvbuf, para.msgSize, 
                    nbr.n_nbr);
            }  

            //printf("rank %d complete computation of loop %d\n", rank, i);
            double cmp_end = MPI_Wtime();
            T_cmp += (cmp_end - cmp_begin);
            if (i <= para.failLoop)
                T_cmp_b += (cmp_end - cmp_begin);
            else
                T_cmp_a += (cmp_end - cmp_begin);

			MPI_Barrier(workComm);	

            double cmm_begin = MPI_Wtime();
            /* communication */
            for (j = 0; j < para.ncomm_neigh; j++) {
                nbr_comm(workComm, &nbr, sendbuf, recvbuf, para.msgSize);
            }
            double neigh_end = MPI_Wtime();
            T_neigh_comm += (neigh_end - cmm_begin);
           
            //printf("rank %d complete neighbor communication of loop %d\n",
            //rank, i);

            double ptp_begin = MPI_Wtime();
            for (j = 0; j < para.ncomm_point; j++) {
                ptp_comm(workComm, sendbuf, recvbuf, para.msgSize);
            }
            double ptp_end = MPI_Wtime();
            T_ptp_comm += (ptp_end - ptp_begin);
            //printf("rank %d complete point to point communication of loop %d\n",
            //rank, i);

            MPI_Barrier(workComm);
            double coll_begin = MPI_Wtime();
            for (j = 0; j < para.ncomm_coll; j++) {
                coll_comm(workComm, sendbuf, recvbuf, para.msgSize);
            }
            double cmm_end = MPI_Wtime();
            //printf("rank %d complete collective communication of loop %d\n",
            //rank, i);
            T_coll_comm += (cmm_end - coll_begin);
            T_cmm += (cmm_end - cmm_begin);
            
			if (i <= para.failLoop)
                T_comm_b += (cmm_end - cmm_begin);
            else
                T_comm_a += (cmm_end - cmm_begin);
        }

        /* At loop para.failLoop, failure occurs, and recovery proceeds */
        if (i == para.failLoop) {
            /* pre-recovery T_b ends */
            end = MPI_Wtime();
            Tb = end - begin;
            MPI_Barrier(MPI_COMM_WORLD);
           
            for (int nfail = 0; nfail < para.nFailure; nfail++) {
                /* deactivate failed rank */
                if (rank == para.failRank[nfail]) {
                    workMem = false;
                    //printf("rank %d failed at loop %d\n", rank, i);
                }

                if (para.recmode == NONSHRINKING) { 
                    //printf("rank %d call NONSHRINKING recovery\n", rank);
                
                    if (spareSize < 1) {
                        fprintf(stderr, "Lack spare ranks.\n");
                        return 1;
                    }
                    newrank = spareRanks[spareSize - 1]; /* subsituted rank */
                    spareSize -= 1;
                
                    /* activate spare rank */
                    if (rank == newrank) {
                        workMem = true;
                        //printf("spareSize = %d, Use spare rank %d\n", spareSize, rank);
                    }

                    /* reconstruct work ranks */
                    for (j = 0; j < workSize; j++) {
                        if (workRanks[j] == para.failRank[nfail]) {
                            workRanks[j] = newrank;
                            /*
                            printf("sub rank %d with spare rank %d\n",
                            para.failRank, newrank);
                            */
                        }
                    }

                } else if (para.recmode == SHRINKING) {
                    workSize -= 1;
                    workRanks = (int *)realloc(workRanks, workSize * sizeof(int));
                    /* reconstruct workRanks by excluding failed rank */
                    for (j = 0, k = 0; j < para.workSize; j++) {
                        if (!is_failRank(j, nfail+1, para.failRank)) {
                            workRanks[k++] = j;
                        }
                    }
                    /* 
                    if (rank == 0) {
                        printf("new workRanks:");
                        for (j = 0; j < workSize; j++) 
                            printf(" %d", workRanks[j]);
                        printf("\n");
                    }*/
                }

                /* reconstruct work group */
                MPI_Group_incl(worldGroup, workSize, workRanks, &workGroup);
                MPI_Comm_create(MPI_COMM_WORLD, workGroup, &workComm);

                /* send the neighbor list of failed rank to spare rank */
                if (para.recmode == NONSHRINKING) {
                    if (rank == para.failRank[nfail]) {
                        MPI_Send(&nbr.n_nbr, 1, MPI_INT,
                                newrank, 0, MPI_COMM_WORLD);

                        MPI_Send(nbrRank, MAXNBR, MPI_INT,
                                newrank, 1, MPI_COMM_WORLD);
                    /*
                    printf("rank %d send neighbor list to rank %d\n", rank, newrank);
                    printf("The list is %d, %d, %d\n", nbrRank[0], nbrRank[1],
                    nbrRank[2]); */

                    } else if (rank == newrank) {
                        MPI_Recv(&nbr.n_nbr, 1, MPI_INT, para.failRank[nfail], 0,
                                MPI_COMM_WORLD, &status);

                        MPI_Recv(nbrRank, MAXNBR, MPI_INT, para.failRank[nfail], 1,
                                MPI_COMM_WORLD, &status);
                        /*
                        printf("rank %d receive neighbor list from rank %d\n", rank,
                        para.failRank);
                        printf("The list is %d, %d, %d\n", nbrRank[0], nbrRank[1],
                        nbrRank[2]); */
                    }
                }

                MPI_Barrier(MPI_COMM_WORLD);

                /* translate neighbor ranks in MPI_COMM_WORLD into workGroup */
                if (workMem) {
                    /* update neighbor list of work group */
                    neighborUpdate(nbrRank, &nbr.n_nbr, worldGroup, workGroup,
                        para.failRank[nfail], newrank, para.recmode);
	            
                    MPI_Group_translate_ranks(worldGroup, nbr.n_nbr, nbrRank,
                        workGroup, nbr.nbr_rank);
                    /*
                    printf("rank %d done translate with neighbor %d\n", rank,
                    nbr.n_nbr);*/
                }

                /* data movement */
                if (para.recmode == NONSHRINKING) {
                    /* Send the array of failed rank to spare rank */
                    if (rank == para.failRank[nfail]) {
                        MPI_Send(array, arraySize, MPI_INT,
                                newrank, 0, MPI_COMM_WORLD);
                    } else if (rank == newrank) {
                        MPI_Recv(array, arraySize, MPI_INT, para.failRank[nfail], 0,
                            MPI_COMM_WORLD, &status);
                    }
                } else if (para.recmode == SHRINKING) {
                    loadBalancer(&array, &arraySize, para.failRank[nfail],
                            workSize, workMem, workRanks, para.lbmode,
                            para.imbl_ratio, &nbr);
                    /*
                    printf("rank %d done load balance, arraySize = %d\n", rank,
                    arraySize); */
                }
               
               MPI_Barrier(MPI_COMM_WORLD);
               //if (rank == 0) printf("Failed rank %d\n", para.failRank[nfail]);
            } /* Multiple failures injected */
            
            /* post-recovery T_a begins */
            begin = MPI_Wtime();
        }
        //printf("finish loop %d\n", i);
    }
    end = MPI_Wtime();
    Ta = end - begin;

    printf("rank %d Tb = %f sec, Ta = %f sec, comp_time = %f sec, "
        "comm_time = %f sec, neigh_comm_time = %f sec, ptp_comm_time = "
        "%f sec, coll_comm_time = %f sec, T_cmp_b = %f sec, T_cmp_a = %f sec, "
        "T_comm_b = %f sec, T_comm_a = %f sec.\n", rank, Tb, Ta, T_cmp, T_cmm,
        T_neigh_comm, T_ptp_comm, T_coll_comm, T_cmp_b, T_cmp_a,
        T_comm_b, T_comm_a);

    free(workRanks);
    free(spareRanks);
    free(array);
    free(sendbuf);
    free(recvbuf);
    free(para.failRank);
    MPI_Group_free(&workGroup);
    MPI_Finalize();

    return 0;
}
