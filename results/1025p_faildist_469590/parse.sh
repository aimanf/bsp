#!/bin/bash

rm *_max
rm NBR_result
rm COLL_result

for stype in NS S
do
	for ctype in NBR COLL
	do
		for i in 1 2 3
		do
			cut -d' ' -f2,5,9,13,17,21,25,29,33,37,41,45 ${stype}_INPUT_${ctype}_${i}.out > ${stype}_${ctype}_${i}
            		cut -d' ' -f2 ${stype}_${ctype}_${i} | sort -nr | head -1 >> ${stype}_${ctype}_${i}_max
            		cut -d' ' -f3 ${stype}_${ctype}_${i} | sort -nr | head -1 >> ${stype}_${ctype}_${i}_max
            		cut -d' ' -f4 ${stype}_${ctype}_${i} | sort -nr | head -1 >> ${stype}_${ctype}_${i}_max
            		cut -d' ' -f5 ${stype}_${ctype}_${i} | sort -nr | head -1 >> ${stype}_${ctype}_${i}_max
            		cut -d' ' -f6 ${stype}_${ctype}_${i} | sort -nr | head -1 >> ${stype}_${ctype}_${i}_max
            		cut -d' ' -f7 ${stype}_${ctype}_${i} | sort -nr | head -1 >> ${stype}_${ctype}_${i}_max
            		cut -d' ' -f8 ${stype}_${ctype}_${i} | sort -nr | head -1 >> ${stype}_${ctype}_${i}_max
            		cut -d' ' -f9 ${stype}_${ctype}_${i} | sort -nr | head -1 >> ${stype}_${ctype}_${i}_max
            		cut -d' ' -f10 ${stype}_${ctype}_${i} | sort -nr | head -1 >> ${stype}_${ctype}_${i}_max
            		cut -d' ' -f11 ${stype}_${ctype}_${i} | sort -nr | head -1 >> ${stype}_${ctype}_${i}_max
            		cut -d' ' -f12 ${stype}_${ctype}_${i} | sort -nr | head -1 >> ${stype}_${ctype}_${i}_max
		done
	done
done

for ctype in NBR COLL
do
	echo "Non-shrinking Ta:" >> ${ctype}_result
	for i in 1 2 3
	do
		head -2 NS_${ctype}_${i}_max | tail -1 >> ${ctype}_result
	done
	echo "Shrinking Ta:" >> ${ctype}_result
	for i in 1 2 3
	do
		head -2 S_${ctype}_${i}_max | tail -1 >> ${ctype}_result
	done
done
