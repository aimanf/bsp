# @ job_name = BSP_4097P
# @ comment = "BSP 4097P Tests"
# @ error = $(job_name).$(jobid).out
# @ output = $(job_name).$(jobid).out
# @ environment = COPY_ALL
# @ wall_clock_limit = 00:10:00
# @ notification = error
# @ notify_user = dun@cs.uchicagoe.du
# @ job_type = bluegene
# @ bg_size = 4097
# @ queue

dirname=../results/4097p_queen_$(date +%y%m%d%H%M%S)
mkdir -p $dirname
cd $dirname

for ctype in NBR COLL; do
    for i in 1 2 3; do
	input=NS_INPUT_${ctype}_${i}
	runjob --np 4097 --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../mira_data/4097p/${input} > ${input}.out
    done
done

for ctype in NBR COLL; do
    for i in 1 2 3; do
	input=S_INPUT_${ctype}_${i}
	runjob --np 4096 --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../mira_data/4097p/${input} > ${input}.out
    done
done

cd ..
