# @ job_name = BSP_1025P
# @ comment = "BSP 1025P Tests"
# @ error = $(job_name).$(jobid).err
# @ output = $(job_name).$(jobid).out
# @ environment = COPY_ALL
# @ wall_clock_limit = 00:10:00
# @ notification = error
# @ notify_user = dun@cs.uchicagoe.du
# @ job_type = bluegene
# @ bg_size = 1025
# @ queue

dirname=../results/1025p_queen_$(date +%y%m%d%H%M%S)
mkdir -p $dirname
cd $dirname

for ctype in NBR COLL; do
    for i in 1 2 3; do
	input=NS_INPUT_${ctype}_${i}
	runjob --np 1025 --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../mira_data/1025p/${input} > ${input}.out
    done
done

for ctype in NBR COLL; do
    for i in 1 2 3; do
	input=S_INPUT_${ctype}_${i}
	runjob --np 1024 --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../mira_data/1025p/${input} > ${input}.out
    done
done

cd ..
