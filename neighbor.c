#include <mpi.h>
#include <stdio.h>
#include "neighbor.h"
#include "inputParser.h"

SPACE translate_rank_to_coordinate(int rank, SPACE space)
{
    /* Translate the rank in workGroup into space coordinates */

    SPACE co;

    co.x = rank % space.x;
    co.y = (rank / space.x) % space.y;
    co.z = rank / (space.x * space.y);

    //printf("Rank %d has coordinates (%d, %d, %d)\n", rank, co.x, co.y, co.z);
    return co;
}

int translate_coordinate_to_rank(SPACE co, SPACE space) 
{
    /* Translate the coordiantes into rank in workGroup */
    int rank;

    rank = co.z * (space.x * space.y);
    
    rank += co.y * space.x;

    rank += co.x;

    return rank;

}

void neighborSetup(NBR *nbr, int *nbrRank, SPACE space, MPI_Group worldGroup,
MPI_Group workGroup)
{
	int rank, size, count = 0;
	MPI_Group_rank(workGroup, &rank);
	MPI_Group_size(workGroup, &size);
    SPACE co; /* My coordinates in the space */

    co = translate_rank_to_coordinate(rank, space);

    /* Check if up side neighbor exists */
    if (co.z + 1 < space.z) {
        SPACE co_up = co;
        co_up.z = co.z + 1;
        int upRank = translate_coordinate_to_rank(co_up, space);
        if (upRank < size)
            nbr->nbr_rank[count++] = upRank;
    }
    /* Check if down side neighbor exists */
    if (co.z - 1 >= 0) {
        SPACE co_down = co;
        co_down.z = co.z - 1;
        int downRank = translate_coordinate_to_rank(co_down, space);
        if (downRank < size)
            nbr->nbr_rank[count++] = downRank; 
    }
    /* Check if front side neighbor exists */
    if (co.y + 1 < space.y) {
        SPACE co_front = co;
        co_front.y = co.y + 1;
        int frontRank = translate_coordinate_to_rank(co_front, space);
        if (frontRank < size)
            nbr->nbr_rank[count++] = frontRank;
    }
    /* Check if behind side neighbor exists */
    if (co.y - 1 >= 0) {
        SPACE co_behind = co;
        co_behind.y = co.y - 1;
        int behindRank = translate_coordinate_to_rank(co_behind, space);
        if (behindRank < size)
            nbr->nbr_rank[count++] = behindRank;
    }
    /* Check if right side neighbor exists */
    if (co.x + 1 < space.x) {
        SPACE co_right = co;
        co_right.x = co.x + 1;
        int rightRank = translate_coordinate_to_rank(co_right, space);
        if (rightRank < size)
            nbr->nbr_rank[count++] = rightRank;
    }
    /* Check if left side neighbor exists */
    if (co.x - 1 >= 0) {
        SPACE co_left = co;
        co_left.x = co.x - 1;
        int leftRank = translate_coordinate_to_rank(co_left, space);
        if (leftRank < size)
            nbr->nbr_rank[count++] = leftRank;
    }

	nbr->n_nbr = count;

	/* translate rank in workGroup to worldGroup */
	MPI_Group_translate_ranks(workGroup, count, nbr->nbr_rank, worldGroup,
			nbrRank);

    /*
    int i;
    printf("Rank %d in workGroup has %d neighbors:", rank, count);
    for (i = 0; i < count; i++ ) {
        printf(" %d", nbr->nbr_rank[i]);
    }
    printf("\n"); */
}

void neighborUpdate(int *nbrRank, int *n_nbr, MPI_Group worldGroup, MPI_Group
workGroup, int fail_rank, int new_rank, RECMODE recovery)
{
	int i, k, count = 0, fooRank[MAXNBR];
    // nbrRank[MAXNBR];
	/* translate neighbor ranks into world group */
    /*
	MPI_Group_translate_ranks(workGroup, MAXNBR, nbr->nbr_rank, worldGroup,
	nbrRank);
    */

	for (i = 0, k = 0; i < *n_nbr; i++) {
		if(nbrRank[i] == fail_rank) {
			if(recovery == NONSHRINKING) {
			/* non-shrinking recovery, replace failed rank with new
			 * rank in neighbor list */
				nbrRank[i] = new_rank;
			}
		} else {
            fooRank[k++] = nbrRank[i];
            count++; /* alive neighbors */
        }
	}

    if (recovery == SHRINKING) {
        for (i = 0; i < count; i++) {
            nbrRank[i] = fooRank[i];
        }
        *n_nbr = count;
    }

	/* translate neighbor ranks into work group */
    /*
	MPI_Group_translate_ranks(worldGroup, MAXNBR, nbrRank, workGroup,
	nbr->nbr_rank); */
}
