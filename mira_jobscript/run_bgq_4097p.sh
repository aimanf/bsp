#!/bin/sh
#COBALT -A GlobalViewResilience -n 4097 -t 20
#COBALT -M aimanf@cs.uchicago.edu 
#COBALT -O ../results/4097p_$COBALT_JOBID

dirname=../results/4097p_${COBALT_JOBID}
mkdir $dirname
cd $dirname

for ctype in NBR COLL; do
    for i in 1 2 3; do
	input=NS_INPUT_${ctype}_${i}
	runjob --block $COBALT_PARTNAME --np 4097 --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../mira_data/4097p/${input} > ${input}.out
    done
done

for ctype in NBR COLL; do
    for i in 1 2 3; do
	input=S_INPUT_${ctype}_${i}
	runjob --block $COBALT_PARTNAME --np 4096 --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../mira_data/4097p/${input} > ${input}.out
    done
done

cd ..
