#!/bin/sh
#COBALT -A GlobalViewResilience -n 1025 -t 29
#COBALT -M aimanf@cs.uchicago.edu 
#COBALT -O ../results/1025p_faildist

dirname=../results/1025p_faildist_${COBALT_JOBID}
mkdir $dirname
cd $dirname

for ctype in NBR COLL; do
	input=FAIL_FREE_${ctype}
	runjob --block $COBALT_PARTNAME --np 1024 --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../mira_data/1025p_faildist/${input} > ${input}.out
done

for ctype in NBR COLL; do
    for i in 1 2 3; do
	input=NS_INPUT_${ctype}_${i}
	runjob --block $COBALT_PARTNAME --np 1025 --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../mira_data/1025p_faildist/${input} > ${input}.out
    done
done

for ctype in NBR COLL; do
    for i in 1 2 3; do
	input=S_INPUT_${ctype}_${i}
	runjob --block $COBALT_PARTNAME --np 1024 --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../mira_data/1025p_faildist/${input} > ${input}.out
    done
done

cd ..
