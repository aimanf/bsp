#include "bsp.h"

void computation(int *array, int arraySize, int *recvbuf, int msgSize, int
n_nbr)
{
    int i, tmp = 0;
    int weight = 0;
    
    /* pseudo calculation */
    for (i = 0; i < n_nbr; i++) {
        weight *= 2; 
    }
    weight %= 23;

    for (i = 0; i < msgSize; i++) {
        tmp += recvbuf[i];
    }

    for (i = 0; i < arraySize; i++) {
        tmp = (array[i] * weight) % 16 + tmp/2;
        array[i]++;
    }
}
