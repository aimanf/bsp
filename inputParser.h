/* A parser for reading parameters from input file */
#ifndef INPUTPARSER_H_
#define INPUTPARSER_H_

typedef enum recovery_mode {
	SHRINKING,
	NONSHRINKING
} RECMODE; /* recovery mode */

typedef enum loadbalance_mode {
	NONE, /* Load is perfectly balanced */
	ONE, /* one outlier */
	SCATTER, /* few scattered outliers */
	GROUP /* few grouped outliers */
} LBMODE; /* load imbalance modes */

typedef struct space_size {
    int x; /* x-axis */
    int y; /* y-axis */
    int z; /* z-axis */
} SPACE; 

typedef struct parameters_st {
    int workSize; /* number of ranks in working group */
    int spareSize; /* number of spare ranks */
    SPACE space; /* workSize = x * y * z */
	int arraySize; /* array size for each rank, which represents the computation
	work */
    int msgSize; /* number of elements in message */
	int maxLoop; /* total number of iterations */
	int failLoop; /* Iteration that a process failure occurs, which determines
	pre-recovery time T_b and post recovery time T_a */
    int nFailure; /* Number of failed nodes */
    int *failRank; /* Failed ranks */
	
	int ncomp; /* computation work iterations, which combining nelem determines the
	computation work */
	
	int ncomm_neigh; /* num of neighbor communication calls */
	int ncomm_point; /* num of point-to-point communication calls */
	int ncomm_coll; /* num of collective calls */

	RECMODE recmode; /* recovery mode */
	
	LBMODE lbmode; /* load imbalance types */
    
    int imbl_ratio; /* load imbalance ratio */
} PARAMETERS;

PARAMETERS processInputs(int argc, char **argv);

#endif
