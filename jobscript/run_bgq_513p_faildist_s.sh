#!/bin/sh
#COBALT -A mc_transport -n 512 -t 10
#COBALT -M aimanf@uchicago.edu
#COBALT -O ../results/513p_faildist_s_$COBALT_JOBID

dirname=../results/513p_faildist_s_${COBALT_JOBID}
mkdir $dirname
cd $dirname

for ctype in NBR COLL; do
    for i in 1 2 3; do
	input=S_INPUT_${ctype}_${i}
	runjob --block $COBALT_PARTNAME --np 512 --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../data/513p_faildist/${input} > ${input}.out
    done
done

cd ..
