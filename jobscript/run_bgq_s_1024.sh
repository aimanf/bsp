#!/bin/sh
#COBALT -A mc_transport -n 1024 -t 30
#COBALT -M aimanf@uchicago.edu
#COBALT -O ../results/ns_1024_$COBALT_JOBID

dirname=../results/s_1024_${COBALT_JOBID}
mkdir $dirname
cd $dirname

for ctype in NBR COLL; do
    for i in 1 2 3; do
	input=S_INPUT_${ctype}_${i}
	runjob --block $COBALT_PARTNAME --np $COBALT_JOBSIZE --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../data/1024p/${input} > ${input}.out
    done
done

cd ..
