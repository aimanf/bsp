#!/bin/sh
#COBALT -A mc_transport -n 257 -t 30
#COBALT -M aimanf@uchicago.edu
#COBALT -O ../results/257p_faildist_$COBALT_JOBID

dirname=../results/257p_faildist_${COBALT_JOBID}
mkdir $dirname
cd $dirname

for ctype in NBR COLL; do
    for i in 1 2 3; do
	input=NS_INPUT_${ctype}_${i}
	runjob --block $COBALT_PARTNAME --np 257 --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../data/257p_faildist/${input} > ${input}.out
    done
done

for ctype in NBR COLL; do
    for i in 1 2 3; do
	input=S_INPUT_${ctype}_${i}
	runjob --block $COBALT_PARTNAME --np 256 --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../data/257p_faildist/${input} > ${input}.out
    done
done

cd ..
