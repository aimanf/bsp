#!/bin/sh
#COBALT -A mc_transport -n 512 -t 30
#COBALT -M aimanf@uchicago.edu
#COBALT -O ../results/faildist_s_$COBALT_JOBID

dirname=../results/faildist_s_${COBALT_JOBID}
mkdir $dirname
cd $dirname

for i in 1 2 3 4 5 6; do
	input=S_INPUT_NBR_${i}
	runjob --block $COBALT_PARTNAME --np 512 --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../data/faildist/${input} > ${input}.out
done

cd ..
