#!/bin/sh
#COBALT -A mc_transport -n 513 -t 30
#COBALT -M aimanf@uchicago.edu
#COBALT -O ../results/faildist_ns_$COBALT_JOBID

dirname=../results/faildist_ns_${COBALT_JOBID}
mkdir $dirname
cd $dirname

for i in 1 2 3 4 5 6; do
	input=NS_INPUT_NBR_${i}
	runjob --block $COBALT_PARTNAME --np 513 --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../data/faildist/${input} > ${input}.out
done

cd ..
