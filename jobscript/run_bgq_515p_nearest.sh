#!/bin/sh
#COBALT -A mc_transport -n 515 -t 10
#COBALT -M aimanf@uchicago.edu
#COBALT -O ../results/515p_nearest_$COBALT_JOBID

dirname=../results/515p_nearest_${COBALT_JOBID}
mkdir $dirname
cd $dirname

for ctype in NBR COLL; do
    for i in 1 2 3; do
	input=NS_INPUT_${ctype}_${i}
	runjob --block $COBALT_PARTNAME --np 515 --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../data/515p_nearest/${input} > ${input}.out
    done
done

for ctype in NBR COLL; do
    for i in 1 2 3; do
	input=S_INPUT_${ctype}_${i}
	runjob --block $COBALT_PARTNAME --np 512 --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../data/515p_nearest/${input} > ${input}.out
    done
done

cd ..
