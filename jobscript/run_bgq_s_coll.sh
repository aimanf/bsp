#!/bin/sh
#COBALT -A mc_transport -n 514 -t 30
#COBALT -M aimanf@uchicago.edu
#COBALT -O ../results/s_coll_$COBALT_JOBID

dirname=../results/s_coll_${COBALT_JOBID}
mkdir $dirname
cd $dirname

for n in 511 512 513; do
	input=S_COLL_${n}p
	runjob --block $COBALT_PARTNAME --np $n --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../data/s_coll/${input} > ${input}.out
done

cd ..
