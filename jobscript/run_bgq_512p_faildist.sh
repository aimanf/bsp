#!/bin/sh
#COBALT -A mc_transport -n 511 -t 20
#COBALT -M aimanf@uchicago.edu
#COBALT -O ../results/512p_faildist_$COBALT_JOBID

dirname=../results/512p_faildist_${COBALT_JOBID}
mkdir $dirname
cd $dirname

export MP_EAGER_LIMIT=32768

for ctype in NBR COLL; do
    for i in 1 2 3; do
	input=S_INPUT_${ctype}_${i}
	runjob --block $COBALT_PARTNAME --np 511 --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../data/512p_faildist/${input} > ${input}.out
    done
done

for ctype in NBR COLL; do
    for i in 1 2 3; do
	input=NS_INPUT_${ctype}_${i}
	runjob --block $COBALT_PARTNAME --np 512 --ranks-per-node 1 --verbose 2 : ../../bsp -f ../../data/512p_faildist/${input} > ${input}.out
    done
done

cd ..
