#ifndef BSP_H
#define BSP_H

#include "neighbor.h"
#include "inputParser.h"
#include <stdbool.h>

void computation(int *array, int arraySize, int *recvbuf, int msgSize, int n_nbr);

/* neighbor communication */
void nbr_comm(MPI_Comm comm, NBR *nbr, int *sendbuf, int *recvbuf, int msgSize);

/* point to point communication */
void ptp_comm(MPI_Comm comm, int *sendbuf, int*recvbuf, int msgSize);

/* collective communication */
void coll_comm(MPI_Comm comm, int *sendbuf, int *recvbuf, int msgSize);

/* load balancer */
void loadBalancer(int **array, int *arraySize, int failRank,
int workSize, bool workMem, int * workRanks, LBMODE recmode, int imbl_ratio,
NBR *nbr);

#endif
