#ifndef NEIGHBOR_H
#define NEIGHBOR_H
#include <mpi.h>
#include "inputParser.h"
// maximum number of neighbors
#define MAXNBR 6 

typedef struct neighbor_st {

	int n_nbr; // number of neighbors
	int nbr_rank[MAXNBR];	// list of neighbor ranks

} NBR;

void neighborSetup(NBR *nbr, int *nbrRank, SPACE space, MPI_Group worldGroup,
MPI_Group workGroup); /* Set up neighbor list */
void neighborUpdate(int *nbrRank, int * n_nbr, MPI_Group worldGroup, MPI_Group
workGroup, int fail_rank, int new_rank, RECMODE recovery); /* Update neighbor
list after failure */

#endif
