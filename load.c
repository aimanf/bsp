#include "bsp.h"
#include <mpi.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

void randomArray(int *array, int arraySize, int *workRanks, int range)
{
    int in, im = 0;
    srand(time(NULL));

    for (in = 0; in < range && im < arraySize; in++) {
        int rn = range - in;
        int rm = arraySize - im;
        if (rand() % rn < rm) {
            //printf("%dth element is chosen\n", in);
            array[im++] = workRanks[in];
        }
    }
    
    /*
    printf("Outliers:");
    for (int i = 0; i < arraySize; i++)
        printf(" %d", array[i]);
    printf("\n");
    */
}

void perfectBL(int **array, int *arraySize, int failRank,
int *workRanks, int workSize, bool workMem)
{
    int rank, i, j;
    MPI_Status status;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int expand = *arraySize / workSize;
    /*
    printf("arraySize = %d, workSize = %d, expand = %d\n", *arraySize,
    workSize, expand);*/
    
    int * tmpArray = (int *)malloc((*arraySize + expand) * sizeof(int));
    int * expandArray = (int *)malloc(expand * sizeof(int));

    if (rank == failRank) {
        for (i = 0; i < workSize; i++) {
            if (workRanks[i] != failRank) {
                for (j = 0; j < expand; j++) {
                    expandArray[j] = (*array)[i*expand + j];
                }
                MPI_Send(expandArray, expand, MPI_INT, workRanks[i], 0,
                MPI_COMM_WORLD);
            }
        }
    } else if (workMem) {
        MPI_Recv(expandArray, expand, MPI_INT, failRank, 0, MPI_COMM_WORLD,
        &status);
        
        for (i = 0; i < *arraySize; i++) {
            tmpArray[i] = (*array)[i];
        }
        for (j = i; j < expand; j++) {
            tmpArray[j] = expandArray[j-i];
        }
        *arraySize += expand;
        (*array) = (int *)realloc(*array, (*arraySize) * sizeof(int)); 
        
        for (i = 0; i < *arraySize; i++) {
            (*array)[i] = tmpArray[i];
        }
        
        free(expandArray);
        free(tmpArray);
    }
}

void oneIMBL(int **array, int *arraySize, int failRank,
int *workRanks, int workSize, bool workMem, int outlier, int imbl_ratio)
{
    int rank, i, j;
    MPI_Status status;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int unit = *arraySize / (workSize + imbl_ratio/100);
    int expand;

    if (rank == failRank || rank == outlier)
        expand = unit * (1 + imbl_ratio/100);
    else
        expand = unit;
    
    int * tmpArray = (int *)malloc((*arraySize + expand) * sizeof(int));
    int * expandArray = (int *)malloc(expand * sizeof(int));

    if (rank == failRank) {
        for (i = 0; i < workSize; i++) {
            if (workRanks[i] == outlier) {
                printf("rank %d is outlier, send %d data\n", workRanks[i],
                expand);
                for (j = 0; j < expand; j++)
                    expandArray[j] = (*array)[i*expand + j];
                MPI_Send(expandArray, expand, MPI_INT, outlier, 0,
                MPI_COMM_WORLD);
            } else if (workRanks[i] != failRank) {
                for (j = 0; j < unit; j++) {
                    expandArray[j] = (*array)[i*expand + j];
                }
                MPI_Send(expandArray, unit, MPI_INT, workRanks[i], 0,
                MPI_COMM_WORLD);
            }
        }
    } else if (workMem) {
            MPI_Recv(expandArray, expand, MPI_INT, failRank, 0, MPI_COMM_WORLD,
        &status);
        
        for (i = 0; i < *arraySize; i++) {
            tmpArray[i] = (*array)[i];
        }
        for (j = i; j < expand; j++) {
            tmpArray[j] = expandArray[j-i];
        }
        *arraySize += expand;
        (*array) = (int *)realloc(*array, (*arraySize) * sizeof(int)); 
        
        for (i = 0; i < *arraySize; i++) {
            (*array)[i] = tmpArray[i];
        }
        
        free(expandArray);
        free(tmpArray);
    }
}

bool is_outlier(int rank, int * outliers, int n_outlier)
{
    int i;
    
    for (i = 0; i < n_outlier; i++) {
        if (rank == outliers[i])
            return true;
    }

    return false;
}

bool is_nonoutlier(int rank, int * nonoutliers, int n_outlier)
{
    int i;
    
    for (i = 0; i < n_outlier; i++) {
        if (rank == nonoutliers[i])
            return true;
    }

    return false;
}

void scatterIMBL(int **array, int *arraySize, int failRank,
int *workRanks, int workSize, bool workMem, int *outliers, int n_outlier, int
imbl_ratio)
{
    int rank, i, j;
    MPI_Status status;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    bool isoutlier = is_outlier(rank, outliers, n_outlier);

    int unit = *arraySize / (workSize + n_outlier*imbl_ratio/100);
    int expand;
    if (rank == failRank || isoutlier == true)
        expand = unit * (1 + imbl_ratio/100);
    else 
        expand = unit;

    int * tmpArray = (int *)malloc((*arraySize + expand) * sizeof(int));
    int * expandArray = (int *)malloc(expand * sizeof(int));

    if (rank == failRank) {
        for (i = 0; i < workSize; i++) {
            if (is_outlier(workRanks[i], outliers, n_outlier)) {
                /* send array to outliers*/
                for (j = 0; j < expand; j++)
                    expandArray[j] = (*array)[i*expand + j];
                MPI_Send(expandArray, expand, MPI_INT, workRanks[i], 0,
                MPI_COMM_WORLD);
            } else if (workRanks[i] != failRank) { 
                /* send array to non-outlires */
                for (j = 0; j < unit; j++) {
                    expandArray[j] = (*array)[i*expand + j];
                }
                MPI_Send(expandArray, unit, MPI_INT, workRanks[i], 0,
                MPI_COMM_WORLD);
            }
        }
    } else if (workMem) {
            MPI_Recv(expandArray, expand, MPI_INT, failRank, 0, MPI_COMM_WORLD,
        &status);
        
        for (i = 0; i < *arraySize; i++) {
            tmpArray[i] = (*array)[i];
        }
        for (j = i; j < expand; j++) {
            tmpArray[j] = expandArray[j-i];
        }
        *arraySize += expand;
        (*array) = (int *)realloc(*array, (*arraySize) * sizeof(int)); 
        
        for (i = 0; i < *arraySize; i++) {
            (*array)[i] = tmpArray[i];
        }
        
        free(expandArray);
        free(tmpArray);
    }
}

void groupIMBL(int **array, int *arraySize, int failRank,
int *workRanks, int workSize, bool workMem, int *outliers, int n_outlier, int
imbl_ratio)
{
    int rank, i, j;
    MPI_Status status;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    bool isoutlier = is_outlier(rank, outliers, n_outlier);

    int unit = *arraySize / (workSize + n_outlier*imbl_ratio/100);
    int expand;
    if (rank == failRank || isoutlier == true)
        expand = unit * (1 + imbl_ratio/100);
    else 
        expand = unit;

    int * tmpArray = (int *)malloc((*arraySize + expand) * sizeof(int));
    int * expandArray = (int *)malloc(expand * sizeof(int));

    if (rank == failRank) {
        for (i = 0; i < workSize; i++) {
            if (is_outlier(workRanks[i], outliers, n_outlier)) {
                /* send array to outliers*/
                for (j = 0; j < expand; j++)
                    expandArray[j] = (*array)[i*expand + j];
                MPI_Send(expandArray, expand, MPI_INT, workRanks[i], 0,
                MPI_COMM_WORLD);
            } else if (workRanks[i] != failRank) { 
                /* send array to non-outlires */
                for (j = 0; j < unit; j++) {
                    expandArray[j] = (*array)[i*expand + j];
                }
                MPI_Send(expandArray, unit, MPI_INT, workRanks[i], 0,
                MPI_COMM_WORLD);
            }
        }
    } else if (workMem) {
            MPI_Recv(expandArray, expand, MPI_INT, failRank, 0, MPI_COMM_WORLD,
        &status);
        
        for (i = 0; i < *arraySize; i++) {
            tmpArray[i] = (*array)[i];
        }
        for (j = i; j < expand; j++) {
            tmpArray[j] = expandArray[j-i];
        }
        *arraySize += expand;
        (*array) = (int *)realloc(*array, (*arraySize) * sizeof(int)); 
        
        for (i = 0; i < *arraySize; i++) {
            (*array)[i] = tmpArray[i];
        }
        
        free(expandArray);
        free(tmpArray);
    }
}

void loadBalancer(int **array, int *arraySize, int failRank,
int workSize, bool workMem, int *workRanks, LBMODE lbmode, int imbl_ratio, NBR
*nbr)
{
    int i, rank, outlier, n_outlier, *outliers;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    switch (lbmode) {
        case NONE:
            perfectBL(array, arraySize, 
            failRank, workRanks, workSize, workMem);
            break;
        case ONE:
            if (rank == failRank) {
                /* decide a random outlier */
                srand(time(NULL));
                int r = rand() % workSize;
                outlier = workRanks[r];
                for (i = 0; i < workSize; i++)
                    MPI_Send(&outlier, 1, MPI_INT, workRanks[i], 0,
                    MPI_COMM_WORLD);
            } else {
                MPI_Status status;
                MPI_Recv(&outlier, 1, MPI_INT, failRank, 0, MPI_COMM_WORLD, &status);
            }
            oneIMBL(array, arraySize, failRank, workRanks, workSize, workMem,
            outlier, imbl_ratio);
            break;
        case SCATTER:
            if (rank == failRank) {
                /* decide the number of scattered outliers */
                srand(time(NULL));
                n_outlier = rand() % workSize + 1;
            }
            MPI_Bcast(&n_outlier, 1, MPI_INT, failRank, MPI_COMM_WORLD);
            outliers = (int *)malloc(n_outlier * sizeof(int));
            
            /* generate a random list of outliers */
            if (rank == failRank) {
                randomArray(outliers, n_outlier, workRanks, workSize);
                /*
                printf("Outliers:");
                for (i = 0; i < n_outlier; i++)
                    printf(" %d", outliers[i]);
                printf("\n"); */
            }
            /* Broadcast the outliers */
            MPI_Bcast(outliers, n_outlier, MPI_INT, failRank, MPI_COMM_WORLD);
            
            scatterIMBL(array, arraySize, failRank, workRanks, workSize,
            workMem, outliers, n_outlier, imbl_ratio);
            
            free(outliers);

            break;
        case GROUP:
            if (rank == failRank) {
                /* decide the center of group outliers */
                srand(time(NULL));
                int r = rand() % workSize;
                outlier = workRanks[r];
            }
            MPI_Bcast(&outlier, 1, MPI_INT, failRank, MPI_COMM_WORLD);

            if (rank == outlier) {
                /* I'm the center of outliers, my neighbors are all outliers */
                n_outlier = nbr->n_nbr + 1;
            }
            MPI_Bcast(&n_outlier, 1, MPI_INT, outlier, MPI_COMM_WORLD);
            outliers = (int *)malloc(n_outlier * sizeof(int));

            if (rank == outlier) {
                outliers[0] = rank;
                for (i = 0; i < nbr->n_nbr; i++)
                    outliers[i+1] = nbr->nbr_rank[i];
            }
            MPI_Bcast(outliers, n_outlier, MPI_INT, outlier, MPI_COMM_WORLD);
            
            groupIMBL(array, arraySize, failRank, workRanks, workSize, workMem,
            outliers, n_outlier, imbl_ratio);
            
            free(outliers);
            
            break;
        
        default:
            break;
    }       
}
