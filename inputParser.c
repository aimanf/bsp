#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "inputParser.h"

PARAMETERS processInputs(int argc, char **argv)
{	
	PARAMETERS para;
	char *inputFile;
    int k;

	inputFile = "input";

	for(int i = 1; i < argc; i++) {
		if ((argv[i] != NULL) && strcmp(argv[i], "-f") == 0) {
			inputFile = argv[++i];
			break;
		}
	}

	FILE *fp = fopen(inputFile, "r");
	if(fp == NULL) {
		printf("Can't open file %s\n", inputFile);
		return para;
	}
	char line[256];
	char *pch;

	fgets(line, sizeof(line), fp);
	pch = strtok(line, " ");
	if (!strcmp(pch, "workSize")) {
		pch = strtok(NULL, " ");
		pch = strtok(NULL, ";");
		para.workSize = atoi(pch);
	} else {
		printf("Miss work size.\n");
	}
	
    fgets(line, sizeof(line), fp);
	pch = strtok(line, " ");
	if (!strcmp(pch, "spareSize")) {
		pch = strtok(NULL, " ");
		pch = strtok(NULL, ";");
		para.spareSize = atoi(pch);
	} else {
		printf("Miss spare size.\n");
	}
    
    fgets(line, sizeof(line), fp);
	pch = strtok(line, " ");
	if (!strcmp(pch, "space")) {
		pch = strtok(NULL, " ");
		pch = strtok(NULL, " ");
		para.space.x = atoi(pch);
		pch = strtok(NULL, " ");
		para.space.y = atoi(pch);
		pch = strtok(NULL, ";");
		para.space.z = atoi(pch);
	} else {
		printf("Miss spare size.\n");
	}
	
    fgets(line, sizeof(line), fp);
	pch = strtok(line, " ");
	if (!strcmp(pch, "array_size")) {
		pch = strtok(NULL, " ");
		pch = strtok(NULL, ";");
		para.arraySize = atoi(pch);
	} else {
		printf("Miss array size.\n");
	}
	
    fgets(line, sizeof(line), fp);
	pch = strtok(line, " ");
	if (!strcmp(pch, "msg_size")) {
		pch = strtok(NULL, " ");
		pch = strtok(NULL, ";");
		para.msgSize = atoi(pch);
	} else {
		printf("Miss msg size.\n");
	}
	
    fgets(line, sizeof(line), fp);
	pch = strtok(line, " ");
	if (!strcmp(pch, "max_loop")) {
		pch = strtok(NULL, " ");
		pch = strtok(NULL, ";");
		para.maxLoop = atoi(pch);
	} else {
		printf("Miss max loop.\n");
	}
	
	fgets(line, sizeof(line), fp);
	pch = strtok(line, " ");
	if (!strcmp(pch, "fail_loop")) {
		pch = strtok(NULL, " ");
		pch = strtok(NULL, ";");
		para.failLoop = atoi(pch);
	} else {
		printf("Miss fail loop.\n");
	}
	
	fgets(line, sizeof(line), fp);
	pch = strtok(line, " ");
	if (!strcmp(pch, "n_fail")) {
		pch = strtok(NULL, " ");
		pch = strtok(NULL, ";");
		para.nFailure = atoi(pch);
	} else {
		printf("Miss number of failures n_fail.\n");
	}

    fgets(line, sizeof(line), fp);
	pch = strtok(line, " ");
	if (!strcmp(pch, "fail_rank")) {
        para.failRank = (int *)malloc(para.nFailure * sizeof(int));
        pch = strtok(NULL, " "); /* extract = */
        for (k = 0; k < para.nFailure - 1; k++) {
		    pch = strtok(NULL, " ");
		    para.failRank[k] = atoi(pch);
        }
		pch = strtok(NULL, ";");
		para.failRank[k] = atoi(pch);
	} else {
		printf("Miss fail rank.\n");
	}
	
	fgets(line, sizeof(line), fp);
	pch = strtok(line, " ");
	if (!strcmp(pch, "computation_loop")) {
		pch = strtok(NULL, " ");
		pch = strtok(NULL, ";");
		para.ncomp = atoi(pch);
	} else {
		printf("Miss commputation loop.\n");
	}
	
	fgets(line, sizeof(line), fp);
	pch = strtok(line, " ");
	if (!strcmp(pch, "comm_neigh_loop")) {
		pch = strtok(NULL, " ");
		pch = strtok(NULL, ";");
		para.ncomm_neigh = atoi(pch);
	} else {
		printf("Miss neighbor communication loop.\n");
	}
	
	fgets(line, sizeof(line), fp);
	pch = strtok(line, " ");
	if (!strcmp(pch, "comm_point_loop")) {
		pch = strtok(NULL, " ");
		pch = strtok(NULL, ";");
		para.ncomm_point = atoi(pch);
	} else {
		printf("Miss point communication loop.\n");
	}
	
	fgets(line, sizeof(line), fp);
	pch = strtok(line, " ");
	if (!strcmp(pch, "comm_coll_loop")) {
		pch = strtok(NULL, " ");
		pch = strtok(NULL, ";");
		para.ncomm_coll = atoi(pch);
	} else {
		printf("Miss collective communication loop.\n");
	}
	
	fgets(line, sizeof(line), fp);
	pch = strtok(line, " ");
	if (!strcmp(pch, "recovery")) {
		pch = strtok(NULL, " ");
		pch = strtok(NULL, ";");
		para.recmode = atoi(pch);
	} else {
		printf("Miss recovery mode. 0: shrink, 1: nonshrink.\n");
	}
	
	fgets(line, sizeof(line), fp);
	pch = strtok(line, " ");
	if (!strcmp(pch, "loadbalance")) {
		pch = strtok(NULL, " ");
		pch = strtok(NULL, ";");
		para.lbmode = atoi(pch);
	} else {
		printf("Miss load balance type.\n");
		printf("0: perfect balance.\n1: one outlier\n2: scatter dist\n3:group dist\n");
	}
	
    fgets(line, sizeof(line), fp);
	pch = strtok(line, " ");
	if (!strcmp(pch, "imbl_ratio")) {
		pch = strtok(NULL, " ");
		pch = strtok(NULL, ";");
		para.imbl_ratio = atoi(pch);
	} else {
		printf("Miss load imbalance ratio.\n");
	}

	return para;
}
