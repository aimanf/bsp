CC = mpicc
CFLAGS = -std=c99 -Wall
INCLUDES = -I.
DEPS = bsp.h inputParser.h neighbor.h load.h
SRCS = computation.c communication.c inputParser.c neighbor.c load.c bsp.c
OBJS = $(SRCS:.c=.o)
TARGET = bsp

.PHONY: depend clean

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(TARGET)

%.o: %.c $(DEPS)
	$(CC) $(CFLAGS) $(INCLUDES) -c $<

clean:
	rm -f *.o *~

depend: $(SRCS)
	makedepend $(INCLUDES) $^
